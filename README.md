# WebdriverIO Project

## How to run

Run the tests with:
```
npm run test
```

## Project Structure
The project is organized as follows:

- pages: contains page objects for interacting with the website
- datos: contains data used in the tests
- test: contains test files
- visual-regression: contains the baseline images for image comparison testing
<br></br>

## Test Cases

### Search product
 
- This test case perfoms a search through an array of products from the 'datos' folder using Data Driven.

### Login

- The first test case attemps to login with a non-existent email account and verifies that an error message is displayed. It also includes visual regression testing for the login button.
- The second test case performs a successfull login and verifies that a welcome message is displayed.


### Language change

- The first test case sets the languange to French and verifies that the search bar placeholder text changes to French.
- The second test case sets the languange to German and verifies that the search bar placeholder text changes to German.
- The third test case sets the languange to English and verifies that the search bar placeholder text changes to English.

### Add product to cart

- The test case searches for a product, enters to the product page and adds the product to the cart, then it verifies that a success message is displayed.

### Logout

- The test case logs in to the website, clicks logout and then verifies that the logout message is displayed.
