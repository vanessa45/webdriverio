import BasePage from './base.page';

class ShoppingCartPage extends BasePage {
    
    //WebElements
    get successMessage(){ return $('.success-msg') }

    //-------------------------------------------------------------------------------------------------------//

    //Methods
    async getSuccessMessageText(){
        return await this.successMessage.getText();
    }

}

export default new ShoppingCartPage();
