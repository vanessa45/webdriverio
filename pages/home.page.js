import BasePage from '../pages/base.page';

class HomePage extends BasePage {
    
    //WebElements
    get languageDropdown(){ return $('#select-language') }
    get accountDropdown(){ return $("//header//span[contains(text(),'Account')]") }
    get logIn(){ return $("//a[contains(text(),'Log In')]") }
    get logOut(){ return $("//a[contains(text(),'Log Out')]") }
    get searchBar(){ return $('#search') }

    //-------------------------------------------------------------------------------------------------------//

    //Methods
    async search(product) {
        addStep(`Buscar artículo: ${product}`);
        await this.searchBar.clearValue();
        await this.searchBar.setValue(product);
        await this.searchBar.keys('Enter');
    }

    async selectLanguage(language) {
        addStep(`Seleccionar lenguaje: ${language}`);
        await this.languageDropdown.selectByVisibleText(language);
    }

    async clickAccountDropdown(){
        await this.accountDropdown.click();
    }
    
    async clickLogIn(){
        addStep(`Hacer click en Log In`);
        await this.clickAccountDropdown();
        await this.logIn.click();
    }

    async clickLogOut(){
        addStep(`Hacer click en Log Out`);
        await this.clickAccountDropdown();
        await this.logOut.click();
    }
    
    async getSearchBarPlaceholderText() {
        return await this.searchBar.getProperty('placeholder');
    }

}

export default new HomePage();
