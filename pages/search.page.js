import BasePage from './base.page';

class SearchPage extends BasePage {
    
    //WebElements
    get resultMessage(){ return $('.page-title') }
    get productImage(){ return $('#product-collection-image-411')}

    //-------------------------------------------------------------------------------------------------------//

    //Methods
    async getResultMessageText(){
        return await this.resultMessage.getText();
    }

    async selectProduct(){
        addStep(`Ingresar al producto`);
        await this.productImage.click();
    }

}

export default new SearchPage();
