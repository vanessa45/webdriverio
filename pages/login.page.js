import BasePage from './base.page';

class LoginPage extends BasePage {
    
    //WebElements
    get email(){ return $('#email') }
    get password(){ return $('#pass') }
    get loginButton(){ return $('#send2') }
    get logOutMessage(){ return $('.page-title')}
    get welcomeMessage(){ return $('.welcome-msg')}
    get errorMessage(){ return $('.error-msg')}

    //-------------------------------------------------------------------------------------------------------//

    //Methods
    async login(mail, pass) {
        addStep(`Ingresar con mail: ${mail} y contraseña: ${pass}`);
        await this.email.setValue(mail);
        await this.password.setValue(pass);
        await this.loginButton.click();
    }

    async getWelcomeMessageText(){
        return await this.welcomeMessage.getText();
    }

    async getErrorMessageText(){
        return await this.errorMessage.getText();
    }

    async getLogOutMessageText(){
        return await this.logOutMessage.getText();
    }

}

export default new LoginPage();
