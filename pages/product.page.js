import BasePage from './base.page';

class ProductPage extends BasePage {
    
    //WebElements
    get addToCartButton(){ return $('.add-to-cart-buttons') }
    get colorDropdown(){ return $('#attribute92')}
    get sizeDropdown(){ return $('#attribute180')}

    //-------------------------------------------------------------------------------------------------------//

    //Methods
    async addProductToCart(){
        addStep(`Añadir producto al carrito`);
        return await this.addToCartButton.click();
    }

    async selectColor(color){
        addStep(`Seleccionar color: ${color}`);
        await this.colorDropdown.selectByVisibleText(color);
    }

    async selectSize(size){
        addStep(`Seleccionar size: ${size}`);
        await this.sizeDropdown.selectByVisibleText(size);
    }

}

export default new ProductPage();
