import homePage from '../pages/home.page';
import productPage from '../pages/product.page';
import searchPage from '../pages/search.page';
import shoppingCartPage from '../pages/shopping.cart.page';


describe('add product', () => { 

    it('add product', async () => {
        await browser.url('/');

        await homePage.search('sweater');
        await searchPage.selectProduct();
        await productPage.selectColor('Red');
        await productPage.selectSize('S');
        await productPage.addProductToCart();

        expect(await shoppingCartPage.getSuccessMessageText()).to.include('was added to your shopping cart');

    });

})