import homePage from '../pages/home.page';
import loginPage from '../pages/login.page';

describe('logout', () => { 

    it('logout', async () => {
        await browser.url('/');

        await homePage.clickLogIn();
        await loginPage.login('sample+1000@gmail.com','123456')
        await homePage.clickLogOut();

        expect(await loginPage.getLogOutMessageText()).to.equal('YOU ARE NOW LOGGED OUT');

    });

})