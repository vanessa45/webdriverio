import homePage from '../pages/home.page';
import loginPage from '../pages/login.page';

describe('login', () => { 

    it('non-existent email account', async () => {
        await browser.url('/');

        await homePage.clickLogIn();

        await loginPage.loginButton.waitForDisplayed();
        expect(
            await browser.checkElement(await loginPage.loginButton, "login button", {
            }), "Error: el botón login de la página no coincide con el original").equal(0);

        await loginPage.login('fruit@gmail.com', '123456');

        expect(await loginPage.getErrorMessageText()).to.equal('Invalid login or password.');

    });

    it('valid login', async () => {
        await browser.url('/');
        
        await homePage.clickLogIn();
        await loginPage.login('sample+1000@gmail.com', '123456');

        expect(await loginPage.getWelcomeMessageText()).to.include('WELCOME');

    });

})