import homePage from '../pages/home.page';
import searchPage from '../pages/search.page';
import DATOS from '../datos/products';

describe('search', () => { 
    DATOS.forEach(({product}) => {
        it(`product search ${product}`, async () => {
            await browser.url('/');

            await homePage.search(product);

            expect(await searchPage.getResultMessageText()).to.include(product);

        });
    });
});
