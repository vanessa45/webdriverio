import homePage from '../pages/home.page';

describe('language change', () => { 

    it('french', async () => {
        await browser.url('/');

        await homePage.selectLanguage('French');

        expect(await homePage.getSearchBarPlaceholderText()).to.equal('Rechercher la boutique entière');

    });

    it('german', async () => {
        await browser.url('/');

        await homePage.selectLanguage('German');

        expect(await homePage.getSearchBarPlaceholderText()).to.equal('Den gesamten Shop durchsuchen...');

    });

    it('english', async () => {
        await browser.url('/');

        await homePage.selectLanguage('English');

        expect(await homePage.getSearchBarPlaceholderText()).to.equal('Search entire store here...');

    });

})